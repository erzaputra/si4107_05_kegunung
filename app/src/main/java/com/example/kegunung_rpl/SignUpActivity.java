package com.example.kegunung_rpl;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kegunung_rpl.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SignUpActivity extends AppCompatActivity {

    EditText mUsername, mPassword, mRepassword, mEmail, mFirstName, mLastName;
    Button mDaftar;
    boolean isSama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        this.mUsername = findViewById(R.id.usernameDaftar);
        this.mPassword = findViewById(R.id.passwordDaftar);
        this.mRepassword = findViewById(R.id.repasswordDaftar);
        this.mEmail = findViewById(R.id.emailDaftar);
        this.mFirstName = findViewById(R.id.firstnameDaftar);
        this.mLastName = findViewById(R.id.lastnameDaftar);
        this.mDaftar = findViewById(R.id.btn_daftar);

        mDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = mUsername.getText().toString();
                String password = mPassword.getText().toString();
                String repassword = mRepassword.getText().toString();
                String email = mEmail.getText().toString();
                String first_name = mFirstName.getText().toString();
                String last_name = mLastName.getText().toString();

                if (username.isEmpty() || username.equals("")){
                    Toast.makeText(SignUpActivity.this, "Username belum diisi!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (password.isEmpty() || password.equals("")){
                    Toast.makeText(SignUpActivity.this, "Password belum diisi!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (repassword.isEmpty() || repassword.equals("")){
                    Toast.makeText(SignUpActivity.this, "Re-Password belum diisi!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (email.isEmpty() || email.equals("")){
                    Toast.makeText(SignUpActivity.this, "Email belum diisi!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (first_name.isEmpty() || first_name.equals("")){
                    Toast.makeText(SignUpActivity.this, "First name belum diisi!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (last_name.isEmpty() || last_name.equals("")){
                    Toast.makeText(SignUpActivity.this, "Last name belum diisi!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!password.equals(repassword)){
                    Toast.makeText(SignUpActivity.this, "Password dan verifikasi tidak sama", Toast.LENGTH_SHORT).show();
                    return;
                }

                daftarProses(username, password, email, first_name, last_name);

            }
        });

    }

    public void login(View view){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    private void daftarProses(final String username,final String password,final String email, final String first_name, final String last_name){
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("admins");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User newUser = new User(
                        "",
                        "",
                        email,
                        first_name,
                        last_name,
                        "",
                        password,
                        username

                );
                ref.child(username).setValue(newUser);
                Toast.makeText(SignUpActivity.this, "Berhasil mendaftar, silahkan login", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
