package com.example.kegunung_rpl.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.verifikasi_activities.ScanBarcode;

import com.example.kegunung_rpl.verifikasi_activities.ListKodeBookingActivity;

public class VerificationFragment extends Fragment {

    ImageButton mScanButton,mListButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_verification, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mScanButton = getView().findViewById(R.id.sucessBtn);
        this.mScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ScanBarcode.class);
                startActivity(intent);
            }
        });

        this.mListButton = getView().findViewById(R.id.ListButton);
        this.mListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ListKodeBookingActivity.class);
                startActivity(intent);
            }
        });
    }



}
