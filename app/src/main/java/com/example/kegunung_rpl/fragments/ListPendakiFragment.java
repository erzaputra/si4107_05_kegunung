package com.example.kegunung_rpl.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.kegunung_rpl.listpendaki_activities.ChoosePendaki;
import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.listpendaki_activities.SearchPendaki;


public class ListPendakiFragment extends Fragment {
    ImageButton mlistpendaki,mdetailpendaki;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_pendaki, container, false);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mlistpendaki = getView().findViewById(R.id.listpendaki);
        this.mlistpendaki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChoosePendaki.class);

                startActivity(intent);
            }
        });

        this.mdetailpendaki = getView().findViewById(R.id.detailpendaki);
        this.mdetailpendaki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SearchPendaki.class);
                startActivity(intent);
            }
        });
    }

}
