package com.example.kegunung_rpl.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.kegunung_rpl.transaksi_activities.ListTransaksi;
import com.example.kegunung_rpl.R;

public class ChooseTransactionFragment extends Fragment {

    ImageButton mSucessBtn, mPendingBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_transaction, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mSucessBtn = getView().findViewById(R.id.sucessBtn);
        this.mSucessBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ListTransaksi.class);
                intent.putExtra("verifyStatus", true);

                startActivity(intent);
            }
        });

        this.mPendingBtn = getView().findViewById(R.id.pendingBtn);
        this.mPendingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ListTransaksi.class);
                intent.putExtra("verifyStatus", false);
                startActivity(intent);
            }
        });
    }
}
