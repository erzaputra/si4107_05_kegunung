package com.example.kegunung_rpl.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.kegunung_rpl.statuspembayaran_activities.ListHistoryActivity;
import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.statuspembayaran_activities.SearchPaymentActivity;


public class StatusPembayaranFragment extends Fragment {

    ImageButton mListButton,mSearchButton;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_status_pembayaran, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mListButton = getView().findViewById(R.id.listButton);
        this.mListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ListHistoryActivity.class);
                startActivity(intent);
            }
        });

        this.mSearchButton = getView().findViewById(R.id.searchButton);
        this.mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SearchPaymentActivity.class);
                startActivity(intent);
            }
        });
    }
}
