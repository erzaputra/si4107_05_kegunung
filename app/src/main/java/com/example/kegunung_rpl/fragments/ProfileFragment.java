package com.example.kegunung_rpl.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.kegunung_rpl.LoginActivity;
import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.fragments.DatePickerFragment;
import com.example.kegunung_rpl.sharedpreference.AppState;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProfileFragment extends Fragment {
    private static final String TAG = "ProfileFragment";

    TextView mBirthday;
    Button mBirthdayButton, mButtonUpdate, mButtonLogout;
    EditText mUsername, mEmail, mFirstName, mLastName, mAddress;

    String date;

    View profileView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        this.profileView = view;
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        referencing();
        this.mBirthdayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        this.mButtonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateData();
            }
        });

        this.mButtonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppState.clearLoggedInUser(getContext());
                startActivity(new
                        Intent(getContext(), LoginActivity.class));
                getActivity().finish();
            }
        });

    }

    public void processDatePickerResult(int year, int month, int day) {
        String month_string = Integer.toString(month + 1);
        String day_string = Integer.toString(day);
        String year_string = Integer.toString(year);
        String dateMessage = (month_string +
                "/" + day_string +
                "/" + year_string);

        this.date = dateMessage;
        this.mBirthday.setText(date);
    }

    public void showDatePicker() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "Tanggal Lahir");
    }

    private void referencing() {
        this.mButtonUpdate = getView().findViewById(R.id.btn_update);
        this.mBirthday = getView().findViewById(R.id.birthdayProfile);
        this.mBirthdayButton = getView().findViewById(R.id.birthdayButton);
        this.mButtonLogout = getView().findViewById(R.id.btn_logout);
        this.mUsername = getView().findViewById(R.id.username);
        this.mEmail = getView().findViewById(R.id.email);
        this.mFirstName = getView().findViewById(R.id.firstname);
        this.mLastName = getView().findViewById(R.id.lastname);
        this.mAddress = getView().findViewById(R.id.address);
        DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("admins").child(AppState.getLoggedInUser(getContext()));
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mUsername.setText(dataSnapshot.child("username").getValue().toString());
                mEmail.setText(dataSnapshot.child("email").getValue().toString());
                mFirstName.setText(dataSnapshot.child("first_name").getValue().toString());
                mLastName.setText(dataSnapshot.child("last_name").getValue().toString());
                mAddress.setText(dataSnapshot.child("alamat").getValue().toString());
                mBirthday.setText(dataSnapshot.child("birthday").getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void updateData() {
        DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("admins").child(AppState.getLoggedInUser(getContext()));
        String firstname = this.mFirstName.getText().toString();
        String lastname = this.mLastName.getText().toString();
        String alamat = this.mAddress.getText().toString();
        String birthday = this.mBirthday.getText().toString();
        db.child("first_name").setValue(firstname);
        db.child("last_name").setValue(lastname);
        db.child("alamat").setValue(alamat);
        db.child("birthday").setValue(birthday);

        Toast.makeText(getContext(), "Berhasil Diubah", Toast.LENGTH_SHORT).show();
        this.referencing();
    }

}
