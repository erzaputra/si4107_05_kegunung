package com.example.kegunung_rpl.verifikasi_activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.kegunung_rpl.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ScanBarcodeVerification extends AppCompatActivity {

    final String kode = null;

    TextView mOrganisasi, mStatusVerify, mStatusNaik;

    String namaOrganisasi = null, deskripsiOrg = null, idOrg = null, kodePesanan = null, nominal = null;
    boolean  statusVerify = false, statusNaik = false;

    String key = null;

    Button mVerifyButton, mDetailBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_barcode_verification);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Verifikasi Pendakian");

        mOrganisasi = findViewById(R.id.organisasi);
        mStatusVerify = findViewById(R.id.statusPembayaran);
        mStatusNaik = findViewById(R.id.statusNaik);
        mVerifyButton = findViewById(R.id.verify_btn);
        mDetailBtn = findViewById(R.id.detail_btn);

        Intent intent = getIntent();

        final String kode = intent.getStringExtra("kode");

        Query q = FirebaseDatabase.getInstance().getReference().child("pesanan").orderByChild("kode").equalTo(kode);
        q.addValueEventListener(new ValueEventListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshotVerifikasiNaik : dataSnapshot.getChildren()) {
                    Log.d("tes", snapshotVerifikasiNaik.getKey());
                    key = snapshotVerifikasiNaik.getKey();
                    namaOrganisasi = snapshotVerifikasiNaik.child("organisasi").child("nama").getValue().toString();
                    deskripsiOrg = snapshotVerifikasiNaik.child("organisasi").child("deskripsi").getValue().toString();
                    idOrg = snapshotVerifikasiNaik.child("organisasi").child("id").getValue().toString();
                    kodePesanan = snapshotVerifikasiNaik.child("kode").getValue().toString();
                    statusVerify = (boolean) snapshotVerifikasiNaik.child("status_verifikasi").getValue();
                    statusNaik = (boolean) snapshotVerifikasiNaik.child("status_naik").getValue();
                }

                mOrganisasi.setText(namaOrganisasi);

                if (statusNaik) {
                    mStatusNaik.setText("Sudah Verifikasi");
                    mStatusNaik.setTextColor(getResources().getColor(R.color.colorPrimary));
                } else {
                    mStatusNaik.setText("Belum Verifikasi");
                    mStatusNaik.setTextColor(getResources().getColor(R.color.colorAccent));
                }

                if (statusVerify) {
                    mStatusVerify.setText("Sudah Verifikasi");
                    mStatusVerify.setTextColor(getResources().getColor(R.color.colorPrimary));

                } else {
                    mStatusVerify.setText("Belum Verifikasi");
                    mStatusVerify.setTextColor(getResources().getColor(R.color.colorAccent));
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mDetailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScanBarcodeVerification.this, DetailNaikActivity.class);
                intent.putExtra("id_organisasi", idOrg);
                intent.putExtra("nama_organisasi", namaOrganisasi);
                intent.putExtra("deskripsi_organisasi", deskripsiOrg);
                startActivity(intent);
            }
        });
    }

    public void verifikasiPendakian(View view) {
        Log.d("key", key);
        DatabaseReference db = FirebaseDatabase.getInstance().getReference();
        db.child("pesanan").child(key).child("status_naik").setValue(true);
        startActivity(new Intent(this, VerificationSuccessActivity.class));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
