package com.example.kegunung_rpl.verifikasi_activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.adapter.ListPendakiNaikAdapter;
import com.example.kegunung_rpl.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DetailNaikActivity extends AppCompatActivity {

    TextView mNamaOrganisasi, mDeskripsiOrganisasi;
    RecyclerView mRecyclerPendaki;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_naik);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Pendaki");
        mNamaOrganisasi = findViewById(R.id.namaOrganisasi);
        mDeskripsiOrganisasi = findViewById(R.id.deskripsiOrganisasi);
        mRecyclerPendaki = findViewById(R.id.recyclerPendaki);

        if (getIntent() != null){
            String id_organisasi = getIntent().getStringExtra("id_organisasi");
            String nama_organisasi = getIntent().getStringExtra("nama_organisasi");
            String deskripsi_organisasi = getIntent().getStringExtra("deskripsi_organisasi");
            this.mNamaOrganisasi.setText(nama_organisasi);
            this.mDeskripsiOrganisasi.setText(deskripsi_organisasi);
            getData(id_organisasi);
        }

    }

    private void getData(String id_organisasi){

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<User> userList = new ArrayList<>();
                for (DataSnapshot data : dataSnapshot.getChildren()){
                    User user = data.getValue(User.class);
                    userList.add(user);
                }

                ListPendakiNaikAdapter adapter = new ListPendakiNaikAdapter(DetailNaikActivity.this,userList);
                LinearLayoutManager layoutManager = new LinearLayoutManager(DetailNaikActivity.this,RecyclerView.VERTICAL,false);
                mRecyclerPendaki.setLayoutManager(layoutManager);
                mRecyclerPendaki.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}