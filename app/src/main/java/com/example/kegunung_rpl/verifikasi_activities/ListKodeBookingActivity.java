package com.example.kegunung_rpl.verifikasi_activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.adapter.ListNaikAdapter;
import com.example.kegunung_rpl.models.Naik;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ListKodeBookingActivity extends AppCompatActivity {

    private static final String TAG = "ListKodeBookingActivity";

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_kode_booking);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("History Pendaki");
        recyclerView = findViewById(R.id.recycleHistory);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        databaseReference = FirebaseDatabase.getInstance().getReference().child("pesanan");

        databaseReference.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Naik> listNaik = new ArrayList<>();
                for (DataSnapshot naikSnapshot : dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: " + naikSnapshot.getValue().toString());
                    Naik naik = naikSnapshot.getValue(Naik.class);
                    if (naik.isStatus_verifikasi())
                        listNaik.add(naik);
                }

                ListNaikAdapter adapter = new ListNaikAdapter(ListKodeBookingActivity.this, listNaik);
                LinearLayoutManager layoutManager = new LinearLayoutManager(ListKodeBookingActivity.this, RecyclerView.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }


}
