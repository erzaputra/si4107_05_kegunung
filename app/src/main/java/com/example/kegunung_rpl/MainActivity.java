package com.example.kegunung_rpl;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.kegunung_rpl.fragments.ChooseTransactionFragment;
import com.example.kegunung_rpl.fragments.ListPendakiFragment;
import com.example.kegunung_rpl.fragments.ProfileFragment;
import com.example.kegunung_rpl.fragments.VerificationFragment;
import com.example.kegunung_rpl.fragments.StatusPembayaranFragment;
import com.example.kegunung_rpl.sharedpreference.AppState;
import com.google.android.material.bottomnavigation.BottomNavigationView;


public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        Fragment fragment = new VerificationFragment();
        loadFragment(fragment);
        bottomNavigationView.setSelectedItemId(R.id.verification_menu);

        if (AppState.getStatusMode(this).equals("day")){
            AppCompatDelegate.setDefaultNightMode
                    (AppCompatDelegate.MODE_NIGHT_NO);
        }else if(AppState.getStatusMode(this).equals("dark")){
            AppCompatDelegate.setDefaultNightMode
                    (AppCompatDelegate.MODE_NIGHT_YES);
        }

    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.mainFragment, fragment).commit();
            return true;
        }
        return false;
    }

    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.profile_menu:
                fragment = new ProfileFragment();
                break;
            case R.id.status_pembayaran_menu:
                fragment = new StatusPembayaranFragment();
                break;
            case R.id.verification_menu:
                fragment = new VerificationFragment();
                break;
        case R.id.transaction:
                fragment = new ChooseTransactionFragment();
                break;
            case R.id.ListPendaki_menu:
                fragment = new ListPendakiFragment();
                break;
        }
        return loadFragment(fragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navbar_navigation, menu);
        int nightMode = AppCompatDelegate.getDefaultNightMode();
        if(nightMode == AppCompatDelegate.MODE_NIGHT_YES){
            menu.findItem(R.id.mode).setTitle("Day Mode");
        } else{
            menu.findItem(R.id.mode).setTitle("Night Mode");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.mode) {
            int nightMode = AppCompatDelegate.getDefaultNightMode();
            if (nightMode == AppCompatDelegate.MODE_NIGHT_YES) {
                AppState.setStatusMode(this,"day");
                AppCompatDelegate.setDefaultNightMode
                        (AppCompatDelegate.MODE_NIGHT_NO);
            } else {
                AppState.setStatusMode(this,"dark");
                AppCompatDelegate.setDefaultNightMode
                        (AppCompatDelegate.MODE_NIGHT_YES);
            }
            recreate();
        }
        return true;
    }
}