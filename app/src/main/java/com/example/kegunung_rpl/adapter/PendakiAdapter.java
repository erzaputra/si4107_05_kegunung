package com.example.kegunung_rpl.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kegunung_rpl.listpendaki_activities.DetailPendakiActivity;
import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.models.Pendaki;

import java.util.List;

public class PendakiAdapter extends RecyclerView.Adapter<PendakiAdapter.MyViewHolder> {


    public Context context;
    List<Pendaki> listPendaki;


    public PendakiAdapter(Context context, List<Pendaki> listPendaki) {
        this.context = context;
        this.listPendaki = listPendaki;
    }

    @NonNull
    @Override
    public PendakiAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_pendaki, parent, false);
        PendakiAdapter.MyViewHolder mViewHolder = new PendakiAdapter.MyViewHolder(mView);
        return mViewHolder;
    }



    @Override
    public void onBindViewHolder(@NonNull PendakiAdapter.MyViewHolder holder, int position) {
        final Pendaki pendakiPosition = this.listPendaki.get(position);
        holder.mPendakiName.setText(pendakiPosition.getFirst_name());
        holder.mUsername.setText(pendakiPosition.getUsername());
        if (pendakiPosition.getKeberkasan().equals("Sudah Lengkap")){
            holder.mStatusPemberkasan.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }
        holder.mStatusPemberkasan.setText(pendakiPosition.getKeberkasan());


        holder.mPendaki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailPendakiActivity.class);
//                intent.putExtra("key", pendakiPosition.getKode());

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listPendaki.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mPendaki;
        TextView mPendakiName, mUsername, mStatusPemberkasan;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mPendaki = itemView.findViewById(R.id.pendaki);
            mPendakiName = itemView.findViewById(R.id.pendakiName);
            mUsername = itemView.findViewById(R.id.pendakiUsername);
            mStatusPemberkasan = itemView.findViewById(R.id.status);
        }

    }

}

