package com.example.kegunung_rpl.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kegunung_rpl.verifikasi_activities.DetailNaikActivity;
import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.models.Naik;

import java.util.List;

public class ListNaikAdapter extends RecyclerView.Adapter<ListNaikAdapter.MyViewHolder> {

    Context context;
    List<Naik> naikList;

    public ListNaikAdapter(Context context, List<Naik> naikList) {
        this.context = context;
        this.naikList = naikList;
    }

    @NonNull
    @Override
    public ListNaikAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_history,parent,false);
        ListNaikAdapter.MyViewHolder holder = new ListNaikAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListNaikAdapter.MyViewHolder holder, int position) {
        final Naik naikPosition = this.naikList.get(position);
        holder.mListOrganisasi.setText(naikPosition.getOrganisasi().getNama().toString());
        holder.mListKodePembayaran.setText(naikPosition.getKode());
        if (naikPosition.isStatus_verifikasi()){
            holder.mListStatusNaik.setText("Sudah Verifikasi");
        }else{
            holder.mListStatusNaik.setText("Belum Verifikasi");
        }

        holder.mCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailNaikActivity.class);
                intent.putExtra("id_organisasi", naikPosition.getOrganisasi().getId().toString());
                intent.putExtra("nama_organisasi", naikPosition.getOrganisasi().getNama().toString());
                intent.putExtra("deskripsi_organisasi", naikPosition.getOrganisasi().getDeskripsi().toString());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return naikList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView mListOrganisasi,mListKodePembayaran,mListStatusNaik;
        CardView mCard;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mListOrganisasi = itemView.findViewById(R.id.listOrganisasi);
            mListKodePembayaran = itemView.findViewById(R.id.listKodePembayaran);
            mListStatusNaik = itemView.findViewById(R.id.listStatusPembayaran);
            mCard = itemView.findViewById(R.id.card_history);

        }
    }
}
