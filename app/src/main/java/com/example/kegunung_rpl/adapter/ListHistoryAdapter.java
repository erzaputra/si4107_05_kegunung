package com.example.kegunung_rpl.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kegunung_rpl.statuspembayaran_activities.DetailOrganisationActivity;
import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.models.Pesanan;

import java.util.List;

public class ListHistoryAdapter extends RecyclerView.Adapter<ListHistoryAdapter.MyViewHolder> {

    Context context;
    List<Pesanan> pesananList;

    public ListHistoryAdapter(Context context, List<Pesanan> pesananList) {
        this.context = context;
        this.pesananList = pesananList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_history,parent,false);
        ListHistoryAdapter.MyViewHolder holder = new ListHistoryAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Pesanan pesananPosition = this.pesananList.get(position);
        holder.mListOrganisasi.setText(pesananPosition.getOrganisasi().getNama().toString());
        holder.mListKodePembayaran.setText(pesananPosition.getKode());
        if (pesananPosition.isStatus_bayar()){
            holder.mListStatusPembayaran.setText("Sudah Bayar");
        }else{
            holder.mListStatusPembayaran.setText("Belum Bayar");
        }

        holder.mCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailOrganisationActivity.class);
                intent.putExtra("id_organisasi", pesananPosition.getOrganisasi().getId().toString());
                intent.putExtra("nama_organisasi", pesananPosition.getOrganisasi().getNama().toString());
                intent.putExtra("deskripsi_organisasi", pesananPosition.getOrganisasi().getDeskripsi().toString());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pesananList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView mListOrganisasi,mListKodePembayaran,mListStatusPembayaran;
        CardView mCard;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mListOrganisasi = itemView.findViewById(R.id.listOrganisasi);
            mListKodePembayaran = itemView.findViewById(R.id.listKodePembayaran);
            mListStatusPembayaran = itemView.findViewById(R.id.listStatusPembayaran);
            mCard = itemView.findViewById(R.id.card_history);

        }
    }
}
