package com.example.kegunung_rpl.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.kegunung_rpl.listpendaki_activities.DetailPendakiActivity;
import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.models.User;
import com.example.kegunung_rpl.statuspembayaran_activities.ProfilePendakiActivity;

import java.util.List;

public class ListPendakiAdapter extends RecyclerView.Adapter<ListPendakiAdapter.MyViewHolder> {

    private Context context;
    private List<User> userList;

    public ListPendakiAdapter(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_pendaki,parent,false);
        ListPendakiAdapter.MyViewHolder holder = new ListPendakiAdapter.MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListPendakiAdapter.MyViewHolder holder, int position) {
        final User userPosition = userList.get(position);
        holder.mNamaPendaki.setText(userPosition.getFirst_name()+" "+userPosition.getLast_name());
        holder.mDetailPendaki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfilePendakiActivity.class);
                intent.putExtra("nama", userPosition.getFirst_name()+" "+userPosition.getLast_name());
                intent.putExtra("tanggal_lahir", userPosition.getBirthday());
                intent.putExtra("asal", userPosition.getAsal());
                intent.putExtra("profesi", userPosition.getProfesi());
                intent.putExtra("keberkasan", userPosition.getKeberkasan());
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView mNamaPendaki;
        Button mDetailPendaki;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mNamaPendaki = itemView.findViewById(R.id.namaPendaki);
            mDetailPendaki = itemView.findViewById(R.id.btn_detailPendaki);

        }
    }
}
