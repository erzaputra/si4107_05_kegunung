package com.example.kegunung_rpl.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.kegunung_rpl.transaksi_activities.PaymentVerification;
import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.models.Verifikasi;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ListTransactionAdapter extends RecyclerView.Adapter<ListTransactionAdapter.MyViewHolder> implements Filterable {


    public Context context;
    List<Verifikasi> listVerify;
    List<Verifikasi> search;


    public ListTransactionAdapter(Context context, List<Verifikasi> listVerify) {
        this.context = context;
        this.listVerify = listVerify;
        this.search = listVerify;

    }

    @NonNull
    @Override
    public ListTransactionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_transaction, parent, false);
        ListTransactionAdapter.MyViewHolder mViewHolder = new ListTransactionAdapter.MyViewHolder(mView);
        return mViewHolder;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Verifikasi transaksiPosition = this.listVerify.get(position);
        holder.mCommunityName.setText(transaksiPosition.getOrganisasi().getNama());
        holder.mTransactionCode.setText(transaksiPosition.getKode());

        if (transaksiPosition.isStatus_bayar()){
            holder.mStatusBayar.setText("Sudah Bayar");
            holder.mStatusBayar.setTextColor(context.getResources().getColor(R.color.colorPrimary));

        }else{
            holder.mStatusBayar.setText("Belum Bayar");
        }

        holder.mTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PaymentVerification.class);
                intent.putExtra("key", transaksiPosition.getKode());

                context.startActivity(intent);
            }
        });

        if(transaksiPosition.isStatus_verifikasi()){
            holder.mStatusBayar.setText("Sudah Verifikasi Pembayaran");
            holder.mStatusBayar.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }
    }

    @Override
    public int getItemCount() {
        return listVerify.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        LinearLayout mTransaction;
        TextView mCommunityName, mTransactionCode, mStatusBayar;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mTransaction = itemView.findViewById(R.id.transaction);
            mCommunityName = itemView.findViewById(R.id.communityName);
            mTransactionCode = itemView.findViewById(R.id.transactionCode);
            mStatusBayar = itemView.findViewById(R.id.status);
        }

    }

    public Filter getFilter() {
        return filterProduct;
    }


    private Filter filterProduct = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Verifikasi> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(search);
                Log.d("apaaja", String.valueOf(filteredList.size()));

            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (Verifikasi item : search) {
                    Log.d("disini2", String.valueOf(item));
                    Log.d("disini3", filterPattern);
                    if (item.getKode().toLowerCase().contains(filterPattern)) {
                        Log.d("cekdisini", String.valueOf(item));
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listVerify.clear();
            listVerify.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

}
