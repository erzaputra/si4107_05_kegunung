package com.example.kegunung_rpl.welcomepages;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kegunung_rpl.R;

public class WelcomePage2Fragment extends Fragment {


    public WelcomePage2Fragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_welcome_page_2, container, false);
    }

}
