package com.example.kegunung_rpl.welcomepages;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.kegunung_rpl.LoginActivity;
import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.sharedpreference.AppState;

public class WelcomePageActivity extends AppCompatActivity {

    int counter = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page);
        Button mNext = findViewById(R.id.slideOneNext);

        moveFragment(counter);
        counter++;

        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (counter == 5){
                    AppState.setStatusOpen(WelcomePageActivity.this,"true");
                    Intent intent = new Intent(WelcomePageActivity.this, LoginActivity.class);
                    startActivity(intent);
                }else{
                    moveFragment(counter);
                    counter++;
                }
            }
        });
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.welcomeFrame, fragment).commit();
            return true;
        }
        return false;
    }

    public boolean moveFragment(int state) {
        Fragment fragment = null;
        switch (state) {
            case 1:
                fragment = new WelcomePage1Fragment();
                break;
            case 2:
                fragment = new WelcomePage2Fragment();
                break;
            case 3:
                fragment = new WelcomePage3Fragment();
                break;
            case 4:
                fragment = new WelcomePage4Fragment();
                break;
        }
        return loadFragment(fragment);
    }
}
