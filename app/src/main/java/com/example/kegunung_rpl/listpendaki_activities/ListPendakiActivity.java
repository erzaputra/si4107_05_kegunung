package com.example.kegunung_rpl.listpendaki_activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.adapter.ListTransactionAdapter;
import com.example.kegunung_rpl.adapter.PendakiAdapter;
import com.example.kegunung_rpl.models.Organisasi;
import com.example.kegunung_rpl.models.Pendaki;
import com.example.kegunung_rpl.models.Verifikasi;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ListPendakiActivity extends AppCompatActivity {

    RecyclerView mRecycleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_pendaki);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("ListPendaki");

        mRecycleView = findViewById(R.id.listClimber);

        getData();


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    public void PendakiAdapter(ArrayList arrayList) {
        PendakiAdapter listTransaksi = new PendakiAdapter(this, arrayList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        mRecycleView.setLayoutManager(linearLayoutManager);
        mRecycleView.setAdapter(listTransaksi);

    }

    private void getData() {
        Intent intent = getIntent();
        String pendakiStatus = intent.getStringExtra("pendakiStatus");
        Toast.makeText(this, pendakiStatus, Toast.LENGTH_SHORT).show();
        Query q = FirebaseDatabase.getInstance().getReference().child("users").orderByChild("keberkasan").equalTo(pendakiStatus);
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<Pendaki> arrayList = new ArrayList();
                for (DataSnapshot snapshotPendaki : dataSnapshot.getChildren()) {
                    Log.d("tes", snapshotPendaki.getKey());
                    String first_name = snapshotPendaki.child("first_name").getValue().toString();
                    String username = snapshotPendaki.child("username").getValue().toString();
                    String keberkasan = snapshotPendaki.child("keberkasan").getValue().toString();

                    Pendaki pendaki = new Pendaki(first_name, username, keberkasan);
                    arrayList.add(pendaki);
                }
                PendakiAdapter(arrayList);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}