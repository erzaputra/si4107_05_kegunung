package com.example.kegunung_rpl.listpendaki_activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.models.Pendaki;
import com.example.kegunung_rpl.models.Pesanan;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SearchPendaki extends AppCompatActivity {
    private static final String TAG = "SearchPendaki";
    EditText mSearchForm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_pendaki);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Seluruh Transaksi");

        this.mSearchForm = findViewById(R.id.searchForm);
    }

    public void check(View view) {
        DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("users");
        db.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String username = mSearchForm.getText().toString();
                boolean isKetemu = false;
                for(DataSnapshot snapshotPendaki : dataSnapshot.getChildren()){
                    if(snapshotPendaki.getValue(Pendaki.class).getUsername().equals(username)){
                        Intent intent = new Intent(SearchPendaki.this, DetailPendakiActivity.class);
                        intent.putExtra("username",username);
                        intent.putExtra("id", snapshotPendaki.getKey().toString());
                        startActivity(intent);
                        isKetemu = true;
                    }
                }
                if (!isKetemu)
                    Toast.makeText(SearchPendaki.this, "Username Tidak Ditemukan!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {


            }
        });
    }
    public void history(View view){
        Intent intent = new Intent(this, DetailPendakiActivity.class);
        startActivity(intent);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
