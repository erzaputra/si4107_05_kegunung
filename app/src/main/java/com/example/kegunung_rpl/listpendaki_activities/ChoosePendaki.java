package com.example.kegunung_rpl.listpendaki_activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.listpendaki_activities.ListPendakiActivity;

public class ChoosePendaki extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_pendaki);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void PendakianTertunda(View view) {
        Toast.makeText(getBaseContext(), "Pendakian Tertunda", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ListPendakiActivity.class);
        intent.putExtra("pendakiStatus", "Belum Lengkap");
        startActivity(intent);
    }

    public void PendakianSukses(View view) {
        Toast.makeText(getBaseContext(), "Pendakian Sukses", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ListPendakiActivity.class);
        intent.putExtra("pendakiStatus", "Sudah Lengkap");
        startActivity(intent);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
