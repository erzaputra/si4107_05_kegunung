package com.example.kegunung_rpl.listpendaki_activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.kegunung_rpl.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;


public class DetailPendakiActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pendaki);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Pendaki");
        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);
        getSupportActionBar().setElevation(0);
        TextView tv_actionbar_title = findViewById(R.id.tv_actionbar_title);
        tv_actionbar_title.setVisibility(View.GONE);
        ImageButton btn_actionbar_back = findViewById(R.id.btn_actionbar_back);
        btn_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Button btn_kartukeluarga = findViewById(R.id.btn_kartukeluarga);
        btn_kartukeluarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kartuKeluarga();
            }
        });

        TextView nama_pendaki_detail = findViewById(R.id.nama_pendaki_detail);
        nama_pendaki_detail.setText(getIntent().getStringExtra("username"));

    }
    private void kartuKeluarga(){
//        startActivity(new Intent(DetailPendakiActivity.this, KartuKeluargaActivity.class));
        Intent kk = new Intent(this, KartuKeluargaActivity.class);
        kk.putExtra("kk", getIntent().getStringExtra("username"));
        startActivity(kk);
    }

    public void detailpendaki(View view) {
        Intent order = new Intent(this, KartuKeluargaActivity.class);
        startActivity(order);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }


}
