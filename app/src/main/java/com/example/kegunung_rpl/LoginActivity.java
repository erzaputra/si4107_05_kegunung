package com.example.kegunung_rpl;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kegunung_rpl.models.Pesanan;
import com.example.kegunung_rpl.models.User;
import com.example.kegunung_rpl.sharedpreference.AppState;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    final String app = "save";

    private ProgressBar pb;

    private EditText mEmail;
    private EditText mPassword;

    private TextView mDaftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        pb = findViewById(R.id.simpleProgressBar);
        pb.setVisibility(View.INVISIBLE);
        mEmail = findViewById(R.id.username_field);
        mPassword = findViewById(R.id.password_field);
        mDaftar = findViewById(R.id.notRegsiter);

        mDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    public void login(View view) {
        switch (view.getId()) {
            case R.id.login_btn:

                if (mEmail.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Email belum terisi", Toast.LENGTH_SHORT).show();
                    return;
                } else if (mPassword.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Password belum terisi", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("admins");
                    db.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for(DataSnapshot snapshotUser : dataSnapshot.getChildren()){
                                User user = snapshotUser.getValue(User.class);
                                if(user.getEmail().equals(mEmail.getText().toString())){
                                    if (!mPassword.getText().toString().equals(user.getPassword())) {
                                        Log.d(TAG, "onDataChange: "+mPassword.getText().toString() + " " + user.getPassword());
                                        Toast.makeText(getBaseContext(), "Password Salah", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    setPreferance(snapshotUser.getValue(User.class).getUsername());
                                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                    return;
                                }
                            }
                            Toast.makeText(getBaseContext(), "Email Tidak Terdaftar", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Toast.makeText(getBaseContext(), "Error DB", Toast.LENGTH_SHORT).show();
                        }

                    });
                }

                break;
        }
    }

    public void setPreferance(String username){
        AppState.setLoggedInUser(getBaseContext(),username);
        AppState.setLoggedInStatus(getBaseContext(),true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AppState.getLoggedInStatus(getBaseContext())){
            startActivity(new Intent(getBaseContext(),MainActivity.class));
            finish();
        }
    }

    /** True jika parameter password sama dengan data password yang terdaftar dari Preferences */
    private boolean cekPassword(String password){
        return password.equals(AppState.getRegisteredPass(getBaseContext()));
    }

    /** True jika parameter user sama dengan data user yang terdaftar dari Preferences */
    private boolean cekUser(String user){
        return user.equals(AppState.getRegisteredUser(getBaseContext()));
    }
}
