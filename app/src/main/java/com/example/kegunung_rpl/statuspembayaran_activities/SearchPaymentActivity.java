package com.example.kegunung_rpl.statuspembayaran_activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.models.Pesanan;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SearchPaymentActivity extends AppCompatActivity {
    private static final String TAG = "SearchPaymentActivity";
    EditText mSearchForm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_payment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cari Transaksi");

        this.mSearchForm = findViewById(R.id.searchForm);
    }

    public void check(View view){

        //inisiani database reference kate golek database sing endi, child itu nggawe ne nang firebase e koyok aku iko
        DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("pesanan");
        db.addValueEventListener(new ValueEventListener() { //iki cek isok kewoco
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) { //method sing dijalanno saat loading
                String kode = mSearchForm.getText().toString(); //kode sing dijupuk teko input text
                boolean isKetemu = false;
                for(DataSnapshot snapshotPesanan : dataSnapshot.getChildren()){ //looping digawe ngecek pesanan / kode
                    //snapshotPesanan.getValue(Pesanan.class).getKode() artine kode teko firebase. Pesanan.class iku sebuah model
                    if(snapshotPesanan.getValue(Pesanan.class).getKode().equals(kode)){ //cabange nang kene
                        Intent intent = new Intent(SearchPaymentActivity.this, DetailPaymentActivity.class);
                        intent.putExtra("code",kode);
                        intent.putExtra("id", snapshotPesanan.getKey().toString());
                        startActivity(intent);
                        isKetemu = true;
                    }
                }
                if (!isKetemu)
                    Toast.makeText(SearchPaymentActivity.this, "Kode Tidak Ditemukan!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void history(View view){
        Intent intent = new Intent(this, ListHistoryActivity.class);
        startActivity(intent);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
