package com.example.kegunung_rpl.statuspembayaran_activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.kegunung_rpl.R;

public class ProfilePendakiActivity extends AppCompatActivity {

    TextView mNama, mAsal, mTanggalLahir, mProfesi, mKeberkasan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_pendaki);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profile Pendaki");

        mNama = findViewById(R.id.namaPendaki);
        mAsal = findViewById(R.id.asal);
        mTanggalLahir = findViewById(R.id.tanggal_lahir);
        mProfesi = findViewById(R.id.profesi);
        mKeberkasan = findViewById(R.id.keberkasan);

        if (getIntent() != null){
            mNama.setText(getIntent().getStringExtra("nama"));
            mAsal.setText(getIntent().getStringExtra("asal"));
            mTanggalLahir.setText(getIntent().getStringExtra("tanggal_lahir"));
            mProfesi.setText(getIntent().getStringExtra("profesi"));
            mKeberkasan.setText(getIntent().getStringExtra("keberkasan"));
        }

    }
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
