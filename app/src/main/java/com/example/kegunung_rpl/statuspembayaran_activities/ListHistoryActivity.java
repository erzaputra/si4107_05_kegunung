package com.example.kegunung_rpl.statuspembayaran_activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.adapter.ListHistoryAdapter;
import com.example.kegunung_rpl.models.Pesanan;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ListHistoryActivity extends AppCompatActivity {
    private static final String TAG = "ListHistoryActivity";

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_history);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("List History");
        recyclerView = findViewById(R.id.recycleHistory);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        databaseReference = FirebaseDatabase.getInstance().getReference().child("pesanan");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Pesanan> listPesanan = new ArrayList<>();
                for (DataSnapshot pesananSnapshot: dataSnapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: "+pesananSnapshot.getValue().toString());
                    Pesanan pesanan = pesananSnapshot.getValue(Pesanan.class);
                    if (pesanan.isStatus_bayar()) {
                        listPesanan.add(pesanan);
                    }
                }

                ListHistoryAdapter adapter = new ListHistoryAdapter(ListHistoryActivity.this,listPesanan);
                LinearLayoutManager layoutManager = new LinearLayoutManager(ListHistoryActivity.this,RecyclerView.VERTICAL,false);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

}
