package com.example.kegunung_rpl.statuspembayaran_activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.kegunung_rpl.MainActivity;
import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.statuspembayaran_activities.DetailOrganisationActivity;
import com.example.kegunung_rpl.transaksi_activities.History;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetailPaymentActivity extends AppCompatActivity {
    private static final String TAG = "DetailPaymentActivity";
    TextView mkodePembayaran, mOrganisasi, mNominal, mPembayaran;

    Button mBackButton;
    String id_organisasi = null, nama_organisasi = null, deskripsi_organisasi = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_payment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Pembayaran");

        mkodePembayaran = findViewById(R.id.kodePembayaran);
        mOrganisasi = findViewById(R.id.organisasi);
        mNominal = findViewById(R.id.nominal);
        mPembayaran = findViewById(R.id.pembayaran);
        mBackButton = findViewById(R.id.backButton);
        getData();

        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void getData(){
        String key = getIntent().getStringExtra("id");
        DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("pesanan").child(key);
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mkodePembayaran.setText(dataSnapshot.child("kode").getValue().toString());
                mOrganisasi.setText(dataSnapshot.child("organisasi").child("nama").getValue().toString());
                mNominal.setText("Rp "+dataSnapshot.child("nominal").getValue().toString()+",-");
                if (dataSnapshot.child("status_bayar").getValue().toString().equals(true)){
                    mPembayaran.setText("Sudah Bayar");
                }else{
                    mPembayaran.setText("Belum Bayar");
                }

                id_organisasi = dataSnapshot.child("organisasi").child("id").getValue().toString();
                nama_organisasi = dataSnapshot.child("organisasi").child("nama").getValue().toString();
                deskripsi_organisasi = dataSnapshot.child("organisasi").child("deskripsi").getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void detail(View view){
        Intent intent = new Intent(this, DetailOrganisationActivity.class);
        intent.putExtra("id_organisasi", id_organisasi);
        intent.putExtra("nama_organisasi", nama_organisasi);
        intent.putExtra("deskripsi_organisasi", deskripsi_organisasi);
        startActivity(intent);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
