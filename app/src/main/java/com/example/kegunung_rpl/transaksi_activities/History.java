package com.example.kegunung_rpl.transaksi_activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.kegunung_rpl.MainActivity;
import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.adapter.HistoryAdapter;
import com.example.kegunung_rpl.adapter.ListTransactionAdapter;
import com.example.kegunung_rpl.models.Organisasi;
import com.example.kegunung_rpl.models.Pesanan;
import com.example.kegunung_rpl.models.Verifikasi;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class History extends AppCompatActivity {

    RecyclerView mRecycleView;

    EditText mSearch;

    ArrayList<Verifikasi> arrayList = new ArrayList();
    HistoryAdapter listHistory = new HistoryAdapter(this, arrayList);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Seluruh Transaksi");

        mRecycleView = findViewById(R.id.listHistory);
        mSearch = findViewById(R.id.searchField);

        getData();

        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(arrayList.size() != 0)
                    listHistory.getFilter().filter(charSequence.toString());
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

    }

    public void HistoryAdapter(ArrayList arrayList) {
        List<Pesanan> list = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL,false);
        mRecycleView.setLayoutManager(linearLayoutManager);
        mRecycleView.setAdapter(listHistory);

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(History.this, MainActivity.class));
        return true;
    }

    private void getData() {
        Intent intent = getIntent();
        boolean verifyStatus = intent.getBooleanExtra("verifyStatus", false);
        Log.d("verifyStatus", String.valueOf(verifyStatus));
        String key = getIntent().getStringExtra("verifyStatus");
        Query q = FirebaseDatabase.getInstance().getReference().child("pesanan");
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean iterator = false;
                for (DataSnapshot snapshotVerifikasi : dataSnapshot.getChildren()) {
                    Log.d("tes", snapshotVerifikasi.getKey());
                    String namaOrganisasi = snapshotVerifikasi.child("organisasi").child("nama").getValue().toString();
                    String deskripsiOrg = snapshotVerifikasi.child("organisasi").child("deskripsi").getValue().toString();
                    String idOrg = snapshotVerifikasi.child("organisasi").child("id").getValue().toString();
                    String kode = snapshotVerifikasi.child("kode").getValue().toString();
                    boolean statusBayar = (boolean) snapshotVerifikasi.child("status_bayar").getValue();
                    boolean statusNaik = (boolean) snapshotVerifikasi.child("status_naik").getValue();
                    boolean statusVerify = (boolean) snapshotVerifikasi.child("status_verifikasi").getValue();

                    Organisasi org = new Organisasi(idOrg, deskripsiOrg,namaOrganisasi);

                    Verifikasi verify = new Verifikasi(kode, org, statusBayar, statusNaik, statusVerify);
                    arrayList.add(verify);
                }
                HistoryAdapter(arrayList);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
