package com.example.kegunung_rpl.transaksi_activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.statuspembayaran_activities.DetailOrganisationActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class PaymentVerification extends AppCompatActivity {
    final String kode = null;

    TextView mOrganisasi, mNominal, mStatusBayar, mStatusVerify;

    String namaOrganisasi = null, deskripsiOrg = null, idOrg = null, kodePesanan = null, nominal = null;
    boolean statusBayar = false, statusVerify = false, statusNaik = false;

    String key = null;

    Button mVerifyButton, mDetailBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_verification);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Verifikasi Pembayaran");

        mOrganisasi = findViewById(R.id.organisasi);
        mNominal = findViewById(R.id.nominal);
        mStatusBayar = findViewById(R.id.statusBayar);
        mStatusVerify = findViewById(R.id.statusVerify);
        mVerifyButton = findViewById(R.id.verify_btn);
        mDetailBtn = findViewById(R.id.detail_btn);

        Intent intent = getIntent();

        final String kode = intent.getStringExtra("key");

        Query q = FirebaseDatabase.getInstance().getReference().child("pesanan").orderByChild("kode").equalTo(kode);
        q.addValueEventListener(new ValueEventListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshotVerifikasi : dataSnapshot.getChildren()) {
                    Log.d("tes", snapshotVerifikasi.getKey());
                    key = snapshotVerifikasi.getKey();
                    namaOrganisasi = snapshotVerifikasi.child("organisasi").child("nama").getValue().toString();
                    deskripsiOrg = snapshotVerifikasi.child("organisasi").child("deskripsi").getValue().toString();
                    idOrg = snapshotVerifikasi.child("organisasi").child("id").getValue().toString();
                    kodePesanan = snapshotVerifikasi.child("kode").getValue().toString();
                    nominal = snapshotVerifikasi.child("nominal").getValue().toString();
                    statusBayar = (boolean) snapshotVerifikasi.child("status_bayar").getValue();
                    statusNaik = (boolean) snapshotVerifikasi.child("status_naik").getValue();
                    statusVerify = (boolean) snapshotVerifikasi.child("status_verifikasi").getValue();
                }

                mOrganisasi.setText(namaOrganisasi);
                mNominal.setText(nominal);

                if (statusBayar) {
                    mStatusBayar.setText("Sudah dibayar");
                    mStatusBayar.setTextColor(getResources().getColor(R.color.colorPrimary));
                } else {
                    mStatusBayar.setText("Belum dibayar");
                    mStatusBayar.setTextColor(getResources().getColor(R.color.colorAccent));
                    mVerifyButton.setVisibility(View.GONE);
                }

                if (statusVerify) {
                    mStatusVerify.setText("Sudah diverifikasi");
                    mStatusVerify.setTextColor(getResources().getColor(R.color.colorPrimary));
                    mVerifyButton.setVisibility(View.GONE);

                } else {
                    mStatusVerify.setText("Belum diverifikasi");
                    mStatusVerify.setTextColor(getResources().getColor(R.color.colorAccent));
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mDetailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentVerification.this, DetailOrganisationActivity.class);
                intent.putExtra("id_organisasi", idOrg);
                intent.putExtra("nama_organisasi", namaOrganisasi);
                intent.putExtra("deskripsi_organisasi", deskripsiOrg);
                startActivity(intent);
            }
        });

    }

    public void proofingOfPayment(View view) {
        Intent intent = new Intent(this, ProofOfPayment.class);
        Log.d("iniKey", key);
        intent.putExtra("key", key);
        intent.putExtra("statusBayar", statusBayar);
        Log.d("sendStatus", String.valueOf(statusVerify));
        intent.putExtra("statusVerifikasi", statusVerify);
        startActivity(intent);

    }

    public void verifikasiPembayaran(View view) {
        startActivity(new Intent(this, History.class));
        Log.d("key", key);
        DatabaseReference db = FirebaseDatabase.getInstance().getReference();
        db.child("pesanan").child(key).child("status_verifikasi").setValue(true);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
