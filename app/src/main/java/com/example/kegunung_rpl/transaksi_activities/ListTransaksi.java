package com.example.kegunung_rpl.transaksi_activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.adapter.ListTransactionAdapter;
import com.example.kegunung_rpl.fragments.ChooseTransactionFragment;
import com.example.kegunung_rpl.models.Organisasi;
import com.example.kegunung_rpl.models.Pesanan;
import com.example.kegunung_rpl.models.Verifikasi;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ListTransaksi extends AppCompatActivity {

    RecyclerView mRecycleView;
    EditText mSearch;

    ArrayList<Verifikasi> arrayList = new ArrayList();

    ListTransactionAdapter listTransaksi = new ListTransactionAdapter(this, arrayList);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_transaksi);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("List Transaksi");

        mRecycleView = findViewById(R.id.listTransaction);
        mSearch = findViewById(R.id.searchField);

        getData();

        mSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(arrayList.size() != 0)
                    listTransaksi.getFilter().filter(charSequence.toString());
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    public void listTransactionAdapter(ArrayList arrayList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        mRecycleView.setLayoutManager(linearLayoutManager);
        mRecycleView.setAdapter(listTransaksi);
            }

    private void getData() {
        Intent intent = getIntent();
        boolean verifyStatus = intent.getBooleanExtra("verifyStatus", false);
        Log.d("verifyStatus", String.valueOf(verifyStatus));

        Query q = FirebaseDatabase.getInstance().getReference().child("pesanan").orderByChild("status_verifikasi").equalTo(verifyStatus);
        q.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean iterator = false;
                for (DataSnapshot snapshotVerifikasi : dataSnapshot.getChildren()) {
                    Log.d("tes", snapshotVerifikasi.getKey());
                    String namaOrganisasi = snapshotVerifikasi.child("organisasi").child("nama").getValue().toString();
                    String deskripsiOrg = snapshotVerifikasi.child("organisasi").child("deskripsi").getValue().toString();
                    String idOrg = snapshotVerifikasi.child("organisasi").child("id").getValue().toString();
                    String kode = snapshotVerifikasi.child("kode").getValue().toString();
                    boolean statusBayar = (boolean) snapshotVerifikasi.child("status_bayar").getValue();
                    boolean statusNaik = (boolean) snapshotVerifikasi.child("status_naik").getValue();
                    boolean statusVerify = (boolean) snapshotVerifikasi.child("status_verifikasi").getValue();

                    Organisasi org = new Organisasi(idOrg, deskripsiOrg, namaOrganisasi);

                    Verifikasi verify = new Verifikasi(kode, org, statusBayar, statusNaik, statusVerify);
                    arrayList.add(verify);
                }
                listTransactionAdapter(arrayList);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
