package com.example.kegunung_rpl.transaksi_activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.transaksi_activities.History;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class ProofOfPayment extends AppCompatActivity {

    ImageView mProofOfTransaction;

    String pesanan;

    boolean statusPembayaran, statusVerify;

    Button mVerifyBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proof_of_payment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Bukti Bayar");

        mProofOfTransaction = findViewById(R.id.setImagePayment);
        mProofOfTransaction.setImageResource(R.drawable.no_image);

        mVerifyBtn = findViewById(R.id.verify_btn);

        Intent intent = getIntent();

        pesanan = intent.getStringExtra("key");
        statusPembayaran = intent.getBooleanExtra("statusBayar",false);
        statusVerify = intent.getBooleanExtra("statusVerifikasi",false);


        Log.d("cekVerifikasi", String.valueOf(statusVerify));
        if (statusVerify) {
            mVerifyBtn.setVisibility(View.GONE);
        }

        loadImage();
    }

    public void verifikasiPembayaran(View view) {
        startActivity(new Intent(this, History.class));
        Log.d("key", pesanan);
        DatabaseReference db = FirebaseDatabase.getInstance().getReference();
        db.child("pesanan").child(pesanan).child("status_verifikasi").setValue(true);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    public void loadImage(){
        FirebaseStorage storage = FirebaseStorage.getInstance();
        Log.d("iniKey", pesanan);
        StorageReference imageRef = storage.getReference().child("buktibayar").child(pesanan).child("buktibayar.jpg");
        imageRef.getBytes(1024*1024).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0, bytes.length);
                mProofOfTransaction.setImageBitmap(bitmap);
            }
        });
    }
}
