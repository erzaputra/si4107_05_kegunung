package com.example.kegunung_rpl.transaksi_activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.kegunung_rpl.R;
import com.example.kegunung_rpl.transaksi_activities.ListTransaksi;

public class ChooseTransaction extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_transaction);
        getSupportActionBar().setTitle("Transaksi");
    }

    public void pendingTransaction(View view) {
        Toast.makeText(getBaseContext(), "Pending Transaction", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, ListTransaksi.class));
    }

    public void successTransaction(View view) {
        Toast.makeText(getBaseContext(), "Success Transaction", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, ListTransaksi.class));
    }


}
