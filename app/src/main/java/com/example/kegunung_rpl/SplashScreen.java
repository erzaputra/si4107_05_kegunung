package com.example.kegunung_rpl;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.kegunung_rpl.sharedpreference.AppState;
import com.example.kegunung_rpl.welcomepages.WelcomePageActivity;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {

            @Override public void run() {
                Intent i;
                if (AppState.getStatusOpen(SplashScreen.this).equals("true")){
                    i = new Intent(SplashScreen.this, LoginActivity.class);
                }else{
                    i = new Intent(SplashScreen.this, WelcomePageActivity.class);
                }
                startActivity(i);

                finish();
            }
            }, 3000);
    }

}
