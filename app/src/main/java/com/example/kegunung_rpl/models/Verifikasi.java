package com.example.kegunung_rpl.models;

public class Verifikasi {

    private String kode;
    private Organisasi organisasi;
    private boolean status_bayar,status_naik, status_verifikasi;

    public Verifikasi(String kode, Organisasi organisasi, boolean status_bayar, boolean status_naik, boolean status_verifikasi) {
        this.kode = kode;
        this.organisasi = organisasi;
        this.status_bayar = status_bayar;
        this.status_naik = status_naik;
        this.status_verifikasi = status_verifikasi;
    }

    public Verifikasi(){} //wajib

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public Organisasi getOrganisasi() {
        return organisasi;
    }

    public void setOrganisasi(Organisasi organisasi) {
        this.organisasi = organisasi;
    }

    public boolean isStatus_bayar() {
        return status_bayar;
    }

    public void setStatus_bayar(boolean status_bayar) {
        this.status_bayar = status_bayar;
    }

    public boolean isStatus_naik() {
        return status_naik;
    }

    public void setStatus_naik(boolean status_naik) {
        this.status_naik = status_naik;
    }

    public boolean isStatus_verifikasi() {
        return status_verifikasi;
    }

    public void setStatus_verifikasi(boolean status_verifikasi) {
        this.status_verifikasi = status_verifikasi;
    }
}
