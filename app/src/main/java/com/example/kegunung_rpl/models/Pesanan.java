package com.example.kegunung_rpl.models;

//model pesanan sing digawe nyimpen data teko firebase
public class Pesanan {

    private String kode;
    private Organisasi organisasi;
    private boolean status_bayar,status_naik;

    public Pesanan(String kode, Organisasi organisasi, boolean status_bayar, boolean status_naik) {
        this.kode = kode;
        this.organisasi = organisasi;
        this.status_bayar = status_bayar;
        this.status_naik = status_naik;
    }

    public Pesanan(){} //wajib

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public Organisasi getOrganisasi() {
        return organisasi;
    }

    public void setOrganisasi(Organisasi organisasi) {
        this.organisasi = organisasi;
    }

    public boolean isStatus_bayar() {
        return status_bayar;
    }

    public void setStatus_bayar(boolean status_bayar) {
        this.status_bayar = status_bayar;
    }

    public boolean isStatus_naik() {
        return status_naik;
    }

    public void setStatus_naik(boolean status_naik) {
        this.status_naik = status_naik;
    }
}
