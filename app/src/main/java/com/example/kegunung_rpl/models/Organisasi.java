package com.example.kegunung_rpl.models;

public class Organisasi {

    private String id, deskripsi, nama;

    public Organisasi(String id, String deskripsi, String nama) {
        this.id = id;
        this.deskripsi = deskripsi;
        this.nama = nama;
    }

    public Organisasi() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
