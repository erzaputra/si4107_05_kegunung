package com.example.kegunung_rpl.models;

public class User {

    private String alamat, birthday, email, first_name, last_name, organisasi, username, asal, keberkasan, profesi, password;

    public User(String alamat, String birthday, String email, String first_name, String last_name, String organisasi,String password, String username) {
        this.alamat = alamat;
        this.birthday = birthday;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.organisasi = organisasi;
        this.username = username;
        this.password = password;
    }

    public User() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAsal() {
        return asal;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public String getKeberkasan() {
        return keberkasan;
    }

    public void setKeberkasan(String keberkasan) {
        this.keberkasan = keberkasan;
    }

    public String getProfesi() {
        return profesi;
    }

    public void setProfesi(String profesi) {
        this.profesi = profesi;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getOrganisasi() {
        return organisasi;
    }

    public void setOrganisasi(String organisasi) {
        this.organisasi = organisasi;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
